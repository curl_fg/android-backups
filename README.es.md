# Android backups
¿Te ha pasado que quieres transferir los datos de tu dispositivo Android a la
PC y te topas que la velocidad a través de MTP es horrible? Probablemente no
sea tu teléfono, [muchos han pasado por
esto](https://www.reddit.com/r/linux/comments/tq8lib/why_is_mtp_such_a_horrible_piece_of_technology/).

Este script te permite copiar tus archivos de manera asíncrona usando las
mismas herramientas que Android proporciona, estoy hablando de la utilidad ADB.

## Preparación
Está de más decir que requiere tener instalado `adb` y que éste sea accesible
desde `PATH`. También es necesario activar las opciones de depuración de
Android en el menú escondido de [opciones de
desarrollador](https://developer.android.com/studio/debug/dev-options) y por
ultimo al conectar el dispositivo a la PC asegúrese de dar los permisos de ADB
y de seleccionar la opción de «Transferencia de archivos».

## Configuración
Puede que los nombres de los directorios a respaldar varíen en cada dispositivo
verifique que estos sean correctos, también puede agregar más directorios o
agregar directorios a excluir.

## Uso
```shellscript
$ ./andybks.sh                  # Hace una copia de sdcard al directorio actual
$ ./andybks.sh target           # Hace una copia de sdcard al directorio target
$ ./andybks.sh a b c            # Hace una copia de a, b y c al directorio actual
$ ./andybks.sh a b c -- target  # Hace una copia de a, b y c al directorio target
```