# Android backups
Have you ever wanted to transfer the data from your Android device to your PC
and you find that the speed through MTP is horrible? It's probably not your
phone, [many have gone through
this](https://www.reddit.com/r/linux/comments/tq8lib/why_is_mtp_such_a_horrible_piece_of_technology/).

This script allows you to copy your files asynchronously using the same tools
that Android provides.  same tools that Android provides, I'm talking about the
ADB utility.

## Preparation
Needless to say, it requires `adb` to be installed and accessible from `PATH`.
You also need to enable the Android debugging options in the hidden [developer
tools](https://developer.android.com/studio/debug/dev-options)
menu and lastly when connecting the device to the PC make sure to give the ADB
permissions and select the «File Transfer» option.

## Configuration
The names of the directories to be backed up may vary for each device.  verify
that these are correct, you can also add more directories or add directories to
exclude.

## Use
```shellscript
$ ./andybks.sh                  # Makes a copy of sdcard to the current directory
$ ./andybks.sh target           # Makes a copy of sdcard to the target directory
$ ./andybks.sh a b c            # Makes a copy of a, b and c to the current directory
$ ./andybks.sh a b c -- target  # Makes a copy of a, b and c to the target directory
```