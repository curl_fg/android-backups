#!/bin/bash
# shellcheck disable=SC2016
declare -a TO_BK=(/sdcard) EXCLUDIRS=('.$Trash$' Android)
declare -i MAX_TASKS=5
# shellcheck disable=SC2155
declare DIR_TO_BK="$PWD/android_bks_$(date +%s)"

if ! command -v adb &>/dev/null; then
	declare -i RTN=$?
	echo "No adb found in PATH" >&2
	exit $RTN
fi

case $# in
	0) ;;

	1) DIR_TO_BK="${*: -1}" ;;

	*)
		TO_BK=()
		while (($# > 0)); do
			if [[ "$1" = -- ]]; then
				shift
				DIR_TO_BK="$1"
				break
			else
				TO_BK+=("$1")
			fi

			shift
		done
		;;
esac

RM_ITM_FROM_ARR() {
	declare -n ARR="$1"
	declare -a TMP

	for I in "${ARR[@]}"; do
		[[ "$2" != "$I" ]] && TMP[${#TMP[@]}]="$I"
	done

	ARR=("${TMP[@]}")
}

for D in "${TO_BK[@]}"; do
	[[ ${D: -1} != / ]] && D+=/

	declare CMD=""
	declare EXCLUDIRSTR=""
	declare TARGET_DIR

	if [[ ${#EXCLUDIRS[@]} -gt 0 ]]; then
		declare -a INDXS=("${!EXCLUDIRS[@]}")
		declare -i LASTINDX="${INDXS[*]: -1}"

		for I in "${INDXS[@]}"; do
			EXCLUDIRSTR+="-path '$D${EXCLUDIRS[$I]}' -prune"
			((I != LASTINDX)) && EXCLUDIRSTR+=' -o '
		done

		EXCLUDIRSTR="\( $EXCLUDIRSTR \) -o -print"
	fi

	CMD="find $D -mindepth 1 -maxdepth 1 -type d $EXCLUDIRSTR"

	mapfile -t < <(adb shell "$CMD")
	RM_ITM_FROM_ARR MAPFILE "$D"

	TARGET_DIR="$DIR_TO_BK/$(basename "$D")"
	mkdir -p "$TARGET_DIR"

	# printf "%s\n" "${MAPFILE[@]}" | xargs -rI% -P $MAX_TASKS echo "% -> $TARGET_DIR"
	printf "%s\n" "${MAPFILE[@]}" | xargs -rI% -P $MAX_TASKS adb pull -a -Z % "$TARGET_DIR"
done
